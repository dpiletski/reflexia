<?php
//If the form is submitted
if(isset($_POST['submit'])) {

	//Check to make sure that the name field is not empty
	if(trim($_POST['name']) == '') {
		$hasError = true;
	} else {
		$name = trim($_POST['name']);
	}

	//Check to make sure that the subject field is not empty
	if(trim($_POST['subject']) == '') {
		$hasError = true;
	} else {
		$subject = trim($_POST['subject']);
	}

	//Check to make sure sure that a valid email address is submitted
	if(trim($_POST['email']) == '')  {
		$hasError = true;
	} else if (!eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($_POST['email']))) {
		$hasError = true;
	} else {
		$email = trim($_POST['email']);
	}

	//Check to make sure comments were entered
	if(trim($_POST['msg']) == '') {
		$hasError = true;
	} else {
		if(function_exists('stripslashes')) {
			$comments = stripslashes(trim($_POST['msg']));
		} else {
			$comments = trim($_POST['msg']);
		}
	}

	//If there is no error, send the email
	if(!isset($hasError)) {
		$emailTo = 'kinzikuta@gmail.com'; //Put your own email address here
		$body = "Name: $name \n\nEmail: $email \n\nSubject: $subject \n\nComments:\n $comments";
		$headers = 'From: My Site <'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email;

		mail($emailTo, $subject, $body, $headers);
		$emailSent = true;
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Reflexia Web Template - Contact Page</title>
<!--
===============
Stylesheets
===============
 -->
<link rel="stylesheet" type="text/css" href="stylepage.css" />
<link rel="stylesheet" type="text/css" href="dropmenu/ddsmoothmenu.css" />
<!--
===============
Favicon
===============
 -->
<link rel="icon" href="img/favicon.ico" type="image/x-icon" />

<!--
===============
Script Files
===============
 -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="jquery-1.2.3.pack.js"></script> 
<script type="text/javascript" src="dropmenu/ddsmoothmenu.js"></script>
<script type="text/javascript" src="js/jquery.slidinglabels.js"></script>
<script type="text/javascript" src="js/sl_action.js"></script>
<script type="text/javascript" src="js/jquery.validate.pack.js"></script>
<script type="text/javascript" src="js/scrolltopcontrol.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script> 
<script type="text/javascript" src="js/Droid_Serif_400.font.js"></script>
<script type="text/javascript"> 
	 Cufon.replace('h1') ('h2') ('h3');</script>
<!--
===========================
Initialize Script Functions
===========================
 -->
<script type="text/javascript">
ddsmoothmenu.init({
	mainmenuid: "smoothmenu1", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})
</script>
<script type="text/javascript">
$(document).ready(function(){
	$("#contactform").validate();

});
</script>
</head>
<body>
<div id="container">
<!--
====================
Header 
====================
 -->  
	<div id="HeaderWrapper"> <!-- Start Header Section -->
    	<div class="left">
    				<div class="logo"> <!-- Start Logo placement here -->
            		<a href="#"><img src="img/reflexia_logo.png" alt="Replace this logo" width="237" height="80" border="0" /></a>
                    </div> <!-- End Logo placement -->
      	</div>
		<div class="right">
        	<div class="searchwrapper"> <!-- Start Search Box here -->
            	<form action="">
                	<input type="text" class="searchbox" name="s" value="Search" onblur="if (this.value == ''){this.value = 'Search'; }" onfocus="if (this.value == 'Search') {this.value = ''; }"/>
                <input type="image" src="img/search_blank.png" class="searchbox_submit" value="" />
            	</form>
           	</div> <!-- End Search Box -->
            
      		<div class="TopMenuWrapper">
        		<div id="smoothmenu1" class="ddsmoothmenu">
                
                		<!-- Start Top Menu Here -->
                        <ul>
                            <li><a href="index.html">HOME</a>
                                <ul>
                                      <li><a href="../darkkhaki/index.html">Skin - Darkkhaki</a></li>
                                      <li><a href="../limegreen/index.html">Skin - Limegreen</a></li>
                                      <li><a href="../palegreen/index.html">Skin - Palegreen</a></li>
                                      <li><a href="../purple/index.html">Skin - Purple</a></li>
                                      <li><a href="../midnightblue/index.html">Skin - Midnightblue</a></li>
                                </ul>
                            </li>
                            <li><a href="about.html">ABOUT</a>
                                <ul>
                                      <li><a href="page_fullwidth.html">Full Width</a></li>
                                      <li><a href="pagestyle.html">Page Style</a></li>
                                      <li><a href="services.html">Service</a></li>
                                      <li><a href="portfolio.html">Portfolio</a></li>
                                      <li><a href="blog.html">Blog</a></li>
                                      <li><a href="contact.php">Contact</a></li>
                                </ul>
                            </li>
                            <li><a href="services.html">SERVICE</a></li>
                            <li><a href="portfolio.html">PORTFOLIO</a> 
                            	<ul>
                                              <li><a href="portfolio.html">Pretty Photo Style</a></li>
                                              <li><a href="portfolio_yoxview.html">YoxView Style</a></li>
                                              <li><a href="portfolio_picbox.html">Picbox Style</a></li>
                            	</ul>
                            </li>
                            <li><a href="blog.html">BLOG</a></li>
                            <li><a href="contact.php">CONTACT</a></li>
                        </ul>
                        <!-- End Top Menu -->
            </div>
      	</div>
     </div> 
    </div> <!-- End Header Section -->
<!--
====================
Page Title
====================
 -->  
    <div id="PageTitleWrapper"> <!-- Start page title section -->
      <h1>CONTACT US</h1>
    </div><!-- End page title section --> 
    
<!--
====================
Content Box 
====================
 -->  
 
    <div id="ContBoxWrapper">
    
    	<!-- Left Section -->
        
   	  <div class="LeftSection"> <!-- Start Content of Left Coloumn Section -->
			<h2>Contact Form</h2>
            <p>If you have any questions or have project in mind, please fill out the form below to contact us. We will be happy to give you information you may need. </p>
       		 <div id="contact-form">
             <?php if(isset($hasError)) { //If errors are found ?>
			 <p class="error">Please check if you've filled all the fields with valid information. Thank you.</p>
	<?php } ?><?php if(isset($emailSent) && $emailSent == true) { //If email is sent ?>
			<p><strong>Email Successfully Sent!</strong></p>
			<p>Thank you <strong><?php echo $name;?></strong> for using my contact form! Your email was successfully sent and I will be in touch with you soon.</p>
	<?php } ?>
       		 <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="contactform">
               <div class="slider">
       		   <label for="name">Name</label>
       		   <input name="name" type="text" id="name" size="35" class="required" />
       		   </div>
               <div class="slider">
       		   <label for="email">Email</label>
       		   <input name="email" type="text" id="email" size="35" class="required email" />
       		   </div>
               <div class="slider">
       		   <label for="subject">Subject</label>
       		   <input name="subject" type="text" id="subject" size="35"/>
               </div>
               <div class="slider">
       		   <label for="msg">Comment</label>
       		   <textarea name="msg" id="msg" cols="40" rows="5" class="required"></textarea>
               </div>
       		   <br />
       		   <label>
       		   <input type="submit" name="submit" id="submit" value="Submit" class="submit"/>
       		   </label>
       		   <br />
       		 </form>
       		 <p>&nbsp;</p>
             </div>
   	  </div> <!-- End Content of Left Section -->
      
      <!-- Right Section -->
      
   	  <div class="RightSection"> <!-- Start Content of Right Section -->
      	<div class="subsection">
          <p>&nbsp;</p>
          <h3>Office Address<br />
       	  </h3>
          <p>12 Decatur Street SE, NSW <br/>
            14 Edgewood Ave, NSW<br />
          Australia          </p>
          <p><img src="img/map.jpg" alt="Location map" width="196" height="210" /><br />
            <br />
          </p>
          <h3>Phone/Fax<br />
              <br />
            </h3>
          +61 1029 939393<br/>
          +61 0200 030303
          <br />
          <br />
          <h3>Email</h3>
            <p>General inquiry: info@company.com <br/>
            Support: support@company.com
          </p>
      	</div>
      </div>
    </div> <!-- End Content of Reft Section -->
    <div class="shadow_box"></div> <!-- box Shadow -->
</div>
<!--
====================
Footer
====================
 -->      
    <div class="clearer"></div>
	<div id="FooterWrapper"> <!-- Start Footer Section -->
    	<div id="inner_div">
        	<div class="footerblock"> <!-- Start Company News block  -->
            	<h3>COMPANY NEWS</h3>
                <p><span class="newsdate">12-08-2010</span>: At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum. <a href="#">..</a><br />
                  <br />
                  <span class="newsdate">10-08-2010</span>: Deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident. Nam libero tempore, cum soluta nobis est eligendi. <a href="#">..</a></p>
          </div> <!-- End Company News block  -->
          
            <div class="footerblock"> <!-- Start Resources block  -->
            	<h3>RESOURCES</h3>
                <div class="linkitems">
                <ul>
                	<li><a href="#">Photoshop Tutorials</a></li>
                    <li><a href="#">PHP Learning Center</a></li>
                    <li><a href="#">Graphic Design Essentials</a></li>
                    <li><a href="#">Free Design Goodies</a></li>
                    <li><a href="#">Quick PC Tips &amp; Tricks</a> </li>
                 </ul>
               </div> 
            </div> <!-- End Resources block  -->
            
            <div class="footerblock"> <!-- Start Contact Us block  -->
            	<h3>CONTACT US</h3>
                <p>Lorem Street, Virginia USA<br/>
                Phone: +1 239049 3939<br />
                Fax: +1 239049 3939<br />
                Email: info@company.com
                </p>
                <p>Connect With Us:</p>
          <div class="SocialWrapper">
                	<ul>
                    		<li><a href="#"><img src="img/social_icons/facebook_32.png" alt="Facebook" /></a></li>
                      <li><a href="#"><img src="img/social_icons/twitter_32.png" alt="Twitter" /></a></li>
                      <li><a href="#"><img src="img/social_icons/flickr_32.png" alt="Flickr" /></a></li>
                      <li><a href="#"><img src="img/social_icons/linkedin_32.png" alt="LinkedID" /></a></li>
                      <li><a href="#"><img src="img/social_icons/rss_32.png" alt="RSS" /></a></li>
            </ul>
                </div>
            </div> <!-- End Contact Us block  -->
        </div>
    </div>  <!-- End Footer Section --> 
    
    <!-- Start Copyright Notice & Bottom Links -->
    <div id="FooterBlackWrapper"> <!-- Start Copyright Notice & Bottom Links -->
    	<div id="inner_div_black">
        		<div class="copyright">Copyright &copy; 2010 - Your Company. All RIghts Reserved.</div>
         		 	<div class="bottomnav">
                            <ul>
                              <li><a href="contact.php">CONTACT</a></li>
                              <li><a href="blog.html">BLOG</a></li>
                              <li><a href="portfolio.html">PORTFOLIO</a></li>
                              <li><a href="services.html">SERVICES</a></li>
                              <li><a href="about.html">ABOUT</a></li>
                              <li><a href="index.html">HOME</a></li>
                            </ul>
          			</div>  
        </div>
    </div> <!-- End Copyright Notice & Bottom Links -->
</body>
</html>
