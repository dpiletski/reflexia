<?php
//If the form is submitted
if(isset($_POST['submit'])) {

	//Check to make sure that the name field is not empty
	if(trim($_POST['name']) == '') {
		$hasError = true;
	} else {
		$name = trim($_POST['name']);
	}

	//Check to make sure that the subject field is not empty
	if(trim($_POST['subject']) == '') {
		$hasError = true;
	} else {
		$subject = trim($_POST['subject']);
	}

	//Check to make sure sure that a valid email address is submitted
	if(trim($_POST['email']) == '')  {
		$hasError = true;
	} else if (!eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($_POST['email']))) {
		$hasError = true;
	} else {
		$email = trim($_POST['email']);
	}

	//Check to make sure comments were entered
	if(trim($_POST['msg']) == '') {
		$hasError = true;
	} else {
		if(function_exists('stripslashes')) {
			$comments = stripslashes(trim($_POST['msg']));
		} else {
			$comments = trim($_POST['msg']);
		}
	}

	//If there is no error, send the email
	if(!isset($hasError)) {
		$emailTo = 'hey.november@gmail.com'; //Put your own email address here
		$body = "Имя: $name \n\nПочта: $email \n\nТема: $subject \n\n Сообщение:\n $comments";
		$headers = 'От ScoringExperts.com <'.$emailTo.'>' . "\r\n" . 'Написать ответ ' . $email;

		mail($emailTo, $subject, $body, $headers);
		$emailSent = true;
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ScoringExperts - Контакты</title>
<!--
===============
Stylesheets
===============
 -->
<link rel="stylesheet" type="text/css" href="stylepage.css" />
<link rel="stylesheet" type="text/css" href="dropmenu/ddsmoothmenu.css" />
<!--
===============
Favicon
===============
 -->
<link rel="icon" href="img/favicon.ico" type="image/x-icon" />

<!--
===============
Script Files
===============
 -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="jquery-1.2.3.pack.js"></script> 
<script type="text/javascript" src="dropmenu/ddsmoothmenu.js"></script>
<script type="text/javascript" src="js/jquery.slidinglabels.js"></script>
<script type="text/javascript" src="js/sl_action.js"></script>
<script type="text/javascript" src="js/jquery.validate.pack.js"></script>
<script type="text/javascript" src="js/scrolltopcontrol.js"></script>
<!--
===========================
Initialize Script Functions
===========================
 -->
<script type="text/javascript">
ddsmoothmenu.init({
	mainmenuid: "smoothmenu1", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})
</script>
<script type="text/javascript">
$(document).ready(function(){
	$("#contactform").validate();

});
</script>
</head>
<body>
<div id="container">
<!--
====================
Header 
====================
 -->  
	<div id="HeaderWrapper"> <!-- Start Header Section -->
    	<div class="left">
    				<div class="logo"> <!-- Start Logo placement here -->
            		<a href="#"><img src="img/logo.png"  width="237" height="80" border="0" /></a>
                    </div> <!-- End Logo placement -->
      	</div>
		<div class="right">
        	 <p>&nbsp;</p>
      		<div class="TopMenuWrapper">
        		<div id="smoothmenu1" class="ddsmoothmenu">
                
                		<!-- Start Top Menu Here -->
                            <ul>
                                <li><a href="main.html">ГЛАВНАЯ</a></li>
                                <li><a href="about.html">О НАС</a></li>
                                <li><a href="services.html">УСЛУГИ</a></li>
                                <li><a href="how_we_work.html">КАК МЫ РАБОТАЕМ</a> </li>
				<li><a href="contact.php">КОНТАКТЫ</a></li>
                        </ul>
                        <!-- End Top Menu -->
            </div>
      	</div>
     </div> 
    </div> <!-- End Header Section -->
<!--
====================
Page Title
====================
 -->  
    <div id="PageTitleWrapper"> <!-- Start page title section -->
      <h1>Связаться с нами</h1>
    </div><!-- End page title section --> 
    
<!--
====================
Content Box 
====================
 -->  
 
    <div id="ContBoxWrapper">
    
    	<!-- Left Section -->
        
   	  <div class="LeftSection"> <!-- Start Content of Left Coloumn Section -->
			<h2>Контакты</h2>
			<br/>
            <p>Для получения дополнительной информации и деловых контактов, вы можете связаться с нами любым удобным для вас способом, в т.ч. заполнив форму ниже: </p>
       		 <div id="contact-form">
             <?php if(isset($hasError)) { //If errors are found ?>
			 <p class="error">Пожалуста, проверьте, чтобы все поля были заполнены корректно</p>
	<?php } ?><?php if(isset($emailSent) && $emailSent == true) { //If email is sent ?>
			<p><strong>Сообщение успешно отправлено!</strong></p>
			<p>Спасибо <strong> <?php echo $name;?>,</strong> за Ваш интерес к компании ScoringExperts!  В ближайшее время мы обязательно свяжемся с Вами.</p>
	<?php } ?>
       		 <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" id="contactform">
               <div class="slider">
       		   <label for="name">Ваше имя*:</label>
       		   <input name="name" Nametype="text" id="name" size="35" class="required" />
       		   </div>
               <div class="slider">
       		   <label for="email">Ваша почта*:</label>
       		   <input name="email" type="text" id="email" size="35" class="required email" />
       		   </div>
               <div class="slider">
       		   <label for="subject">Тема:</label>
       		   <input name="subject" type="text" id="subject" size="35"/>
               </div>
               <div class="slider">
       		   <label for="msg">Ваше сообщение*:</label>
       		   <textarea name="msg" id="msg" cols="50" rows="10" class="required"></textarea>
               </div>
       		   <br />
       		   <label>
       		   <input type="submit" name="submit" id="submit" value="Отправить" class="submit"/>
       		   </label>
       		   <br />
       		 </form>
       		 <p>&nbsp;</p>
             </div>
   	  </div> <!-- End Content of Left Section -->
      
      <!-- Right Section -->
      
   	  <div class="RightSection"> <!-- Start Content of Right Section -->
      	<div class="subsection">
          <p>&nbsp;</p>
	  <br/><br/>
	  <p><img src="img/skype-icon.png"  width="60" height="60" class="imgright"/></p>
	<h3>Skype:<br />
              <br />
            </h3>
           <strong>wwwscoring</strong><br/>
          <br />
          <br />
	  <p><img src="img/phone-icon.png"  width="60" height="60" class="imgright"/></p>
	   <h3>Телефон: <br />
              <br />
            </h3>
           <strong>+1 (330) 741-1288</strong><br/>
	   <br />
          <br/>
	  <p><img src="img/mail-icon.png"  width="60" height="60" class="imgright"/></p>
          <h3>Email: </h3>
            <p>scoringexperts@gmail.com
          </p>
      	</div>
      </div>
    </div> <!-- End Content of Reft Section -->
    <div class="shadow_box"></div> <!-- box Shadow -->
</div>
    
    <div class="clearer"></div>

    <div id="FooterBlackWrapper"> <!-- Start Copyright Notice & Bottom Links -->
    	<div id="inner_div_black">
        		<div class="copyright">&copy; 2014 - ScoringExperts. Все права защищены.</div>
         		 	<div class="bottomnav">
                            <ul>
                               <li><a href="main.html">ГЛАВНАЯ</a></li>
                                <li><a href="about.html">О НАС</a></li>
                                <li><a href="services.html">УСЛУГИ</a></li>
                                <li><a href="how_we_work.html">КАК МЫ РАБОТАЕМ</a> </li>
				<li><a href="contact.php">КОНТАКТЫ</a></li>
                            </ul>
          			</div>  
        </div>
</div> <!-- End Copyright Notice & Button Links -->
</body>
</html>